
"use strict";

let SetUVCWhiteBalance = require('./SetUVCWhiteBalance.js')
let SetUVCExposure = require('./SetUVCExposure.js')
let SwitchIRCamera = require('./SwitchIRCamera.js')
let GetUVCGain = require('./GetUVCGain.js')
let SetUVCGain = require('./SetUVCGain.js')
let SetIRGain = require('./SetIRGain.js')
let SetIRFlood = require('./SetIRFlood.js')
let GetIRGain = require('./GetIRGain.js')
let SetLDP = require('./SetLDP.js')
let ResetIRExposure = require('./ResetIRExposure.js')
let GetDeviceType = require('./GetDeviceType.js')
let SetIRExposure = require('./SetIRExposure.js')
let GetUVCWhiteBalance = require('./GetUVCWhiteBalance.js')
let GetUVCExposure = require('./GetUVCExposure.js')
let GetSerial = require('./GetSerial.js')
let GetCameraInfo = require('./GetCameraInfo.js')
let SetLaser = require('./SetLaser.js')
let GetIRExposure = require('./GetIRExposure.js')
let ResetIRGain = require('./ResetIRGain.js')

module.exports = {
  SetUVCWhiteBalance: SetUVCWhiteBalance,
  SetUVCExposure: SetUVCExposure,
  SwitchIRCamera: SwitchIRCamera,
  GetUVCGain: GetUVCGain,
  SetUVCGain: SetUVCGain,
  SetIRGain: SetIRGain,
  SetIRFlood: SetIRFlood,
  GetIRGain: GetIRGain,
  SetLDP: SetLDP,
  ResetIRExposure: ResetIRExposure,
  GetDeviceType: GetDeviceType,
  SetIRExposure: SetIRExposure,
  GetUVCWhiteBalance: GetUVCWhiteBalance,
  GetUVCExposure: GetUVCExposure,
  GetSerial: GetSerial,
  GetCameraInfo: GetCameraInfo,
  SetLaser: SetLaser,
  GetIRExposure: GetIRExposure,
  ResetIRGain: ResetIRGain,
};
