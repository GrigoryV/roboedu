#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/grisha/catkin_ws/devel/.private/pubsub:$CMAKE_PREFIX_PATH"
export PWD="/home/grisha/catkin_ws/build/pubsub"
export ROSLISP_PACKAGE_DIRECTORIES="/home/grisha/catkin_ws/devel/.private/pubsub/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/grisha/catkin_ws/src/pubsub/src:$ROS_PACKAGE_PATH"